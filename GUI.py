import sys
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow
import os
import RAKE

Form = uic.loadUiType(os.path.join(os.getcwd(), 'test.ui'))[0]


class Photo(Form, QMainWindow):
    def __init__(self):
        Form.__init__(self)
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.pushButton.clicked.connect(self.press)
        for file in os.listdir():
            if file.endswith(".txt"):
                self.comboBox.addItem(file)

    def press(self):
        self.textEdit.clear()
        valueOfComboBox = str(self.comboBox.currentText())
        with open(valueOfComboBox, "r") as file:
            text = file.read()
            RAKE_STOPLIST = 'stoplists/SmartStoplist.txt'
            rake = RAKE.Rake(RAKE_STOPLIST, min_char_length=2, max_words_length=5)
            t = rake.run(text)
            for i in t:
                print(i)
                self.textEdit.append(str(i))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = Photo()
    w.show()
    sys.exit(app.exec_())
